<?php

namespace app\modules\v1\models;

use Yii;

/**
 * This is the model class for table "ticket".
 *
 * @property int $id
 * @property int $row
 * @property int $place
 * @property int $is_blocked
 * @property int $is_bought
 */
class Ticket extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ticket';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['row', 'place', 'is_blocked', 'is_bought'], 'integer'],
            [['row', 'place'], 'unique', 'targetAttribute' => ['row', 'place']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'row' => 'Row',
            'place' => 'Place',
            'is_blocked' => 'Is Blocked',
            'is_bought' => 'Is Bought',
        ];
    }
}

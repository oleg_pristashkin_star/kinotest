<?php

namespace app\modules\v1\controllers;

use app\models\Ticket;

class TicketController extends \yii\web\Controller
{
    public $enableCsrfValidation = false;
    /**
     * @api {get} /get-tickets/ Request all tickets
     * @apiName GetTickets
     * @apiGroup Tickets
     *
     *
     * @apiSuccess {Boolean} status status of operation.
     * @apiSuccess {Object} tickets  the list of tickets.
     */
    public function actionGetTickets()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $tickets = \app\modules\v1\models\Ticket::find()->all();
        return [
            'status' => true,
            'tickets' => $tickets
        ];
    }

    /**
     * @api {get} /get-tickets/:id Request single ticket
     * @apiName GetTicket
     * @apiGroup Tickets
     *
     * @apiParam {Number} id Tickets unique ID.
     *
     * @apiSuccess {Boolean} status the status of operation.
     * @apiSuccess {Object} ticket  a ticket object.
     */
    public function actionGetTicket()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $id = \Yii::$app->request->post('id');
        if(is_null($id)){
            return [
                'status' => false,
                'message' => 'No ID'
            ];
        }

        $ticket = \app\modules\v1\models\Ticket::find($id)->one();
        if($ticket){
            return [
                'status' => true,
                'tickets' => $ticket
            ];
        } else{
            return [
                'status' => false,
                'message' => 'No ticket found'
            ];
        }

    }


    /**
     * @api {patch} /book-ticket/:id book the ticket
     * @apiName BookTicket
     * @apiGroup Tickets
     *
     * @apiParam {Number} id Tickets unique ID.
     *
     * @apiSuccess {Boolean} status the status of operation.
     * @apiSuccess {Object} ticket  a ticket object.
     */
    public function actionBookTicket()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $id = \Yii::$app->request->post('id');
        if(is_null($id)){
            return [
                'status' => false,
                'message' => 'No ID'
            ];
        }
        $ticket = \app\modules\v1\models\Ticket::find($id)->one();
        if(!$ticket){
            return [
                'status' => false,
                'message' => 'No ticket found'
            ];
        } else if($ticket->is_blocked) {
            return [
                'status' => false,
                'message' => 'ticket is blocked'
            ];
        } else{
            $ticket->is_blocked = 1;
            $ticket->save();
            return [
                'status' => true,
                'tickets' => $ticket
            ];
        }
    }

    /**
     * @api {patch} /unbook-ticket/:id unbook the ticket
     * @apiName UnbookTicket
     * @apiGroup Tickets
     *
     * @apiParam {Number} id Tickets unique ID.
     *
     * @apiSuccess {Boolean} status the status of operation.
     * @apiSuccess {Object} ticket  a ticket object.
     */
    public function actionUnbookTicket()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $id = \Yii::$app->request->post('id');

        if(!$id){
            return [
                'status' => false,
                'message' => 'No ID'
            ];
        }

        $ticket = \app\modules\v1\models\Ticket::find($id)->one();
        if(!$ticket){
            return [
                'status' => false,
                'message' => 'No ticket found'
            ];
        } else if(!$ticket->is_blocked) {
            return [
                'status' => false,
                'message' => 'ticket is not blocked'
            ];
        } else{
            $ticket->is_blocked = 0;
            $ticket->save();
            return [
                'status' => true,
                'tickets' => $ticket
            ];
        }
    }

    /**
     * @api {patch} /buy-ticket/:id buy the ticket
     * @apiName BuyTicket
     * @apiGroup Tickets
     *
     * @apiParam {Number} id Tickets unique ID.
     *
     * @apiSuccess {Boolean} status the status of operation.
     * @apiSuccess {Object} ticket  a ticket object.
     */
    public function actionBuyTicket()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $id = \Yii::$app->request->post('id');

        if(!$id){
            return [
                'status' => false,
                'message' => 'No ID'
            ];
        }

        $ticket = \app\modules\v1\models\Ticket::find($id)->one();
        if(!$ticket){
            return [
                'status' => false,
                'message' => 'No ticket found'
            ];
        } else if($ticket->is_bought) {
            return [
                'status' => false,
                'message' => 'ticket is already bought'
            ];
        } else{
            $ticket->is_bought = 1;
            $ticket->save();
            return [
                'status' => true,
                'tickets' => $ticket
            ];
        }
    }


}

define({ "api": [
  {
    "type": "patch",
    "url": "/book-ticket/:id",
    "title": "book the ticket",
    "name": "BookTicket",
    "group": "Tickets",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Tickets unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>the status of operation.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "ticket",
            "description": "<p>a ticket object.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./modules/v1/controllers/TicketController.php",
    "groupTitle": "Tickets"
  },
  {
    "type": "patch",
    "url": "/buy-ticket/:id",
    "title": "buy the ticket",
    "name": "BuyTicket",
    "group": "Tickets",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Tickets unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>the status of operation.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "ticket",
            "description": "<p>a ticket object.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./modules/v1/controllers/TicketController.php",
    "groupTitle": "Tickets"
  },
  {
    "type": "get",
    "url": "/get-tickets/:id",
    "title": "Request single ticket",
    "name": "GetTicket",
    "group": "Tickets",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Tickets unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>the status of operation.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "ticket",
            "description": "<p>a ticket object.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./modules/v1/controllers/TicketController.php",
    "groupTitle": "Tickets"
  },
  {
    "type": "get",
    "url": "/get-tickets/",
    "title": "Request all tickets",
    "name": "GetTickets",
    "group": "Tickets",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>status of operation.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "tickets",
            "description": "<p>the list of tickets.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./modules/v1/controllers/TicketController.php",
    "groupTitle": "Tickets"
  },
  {
    "type": "patch",
    "url": "/unbook-ticket/:id",
    "title": "unbook the ticket",
    "name": "UnbookTicket",
    "group": "Tickets",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Tickets unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>the status of operation.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "ticket",
            "description": "<p>a ticket object.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./modules/v1/controllers/TicketController.php",
    "groupTitle": "Tickets"
  },
  {
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "varname1",
            "description": "<p>No type.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "varname2",
            "description": "<p>With type.</p>"
          }
        ]
      }
    },
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "./doc/main.js",
    "group": "_var_www_testtaxi_doc_main_js",
    "groupTitle": "_var_www_testtaxi_doc_main_js",
    "name": ""
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/almasaeed2010/adminlte/bower_components/mocha/mocha.js",
    "group": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "groupTitle": "_var_www_testtaxi_vendor_almasaeed2010_adminlte_bower_components_mocha_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "@param",
    "title": "$method",
    "version": "0.0.0",
    "filename": "./vendor/codeception/base/src/Codeception/Lib/InnerBrowser.php",
    "group": "_var_www_testtaxi_vendor_codeception_base_src_Codeception_Lib_InnerBrowser_php",
    "groupTitle": "_var_www_testtaxi_vendor_codeception_base_src_Codeception_Lib_InnerBrowser_php",
    "name": "Param"
  },
  {
    "type": "",
    "url": "@param",
    "title": "$method",
    "version": "0.0.0",
    "filename": "./vendor/codeception/base/src/Codeception/Lib/InnerBrowser.php",
    "group": "_var_www_testtaxi_vendor_codeception_base_src_Codeception_Lib_InnerBrowser_php",
    "groupTitle": "_var_www_testtaxi_vendor_codeception_base_src_Codeception_Lib_InnerBrowser_php",
    "name": "Param"
  },
  {
    "type": "",
    "url": "@return",
    "title": "string",
    "version": "0.0.0",
    "filename": "./vendor/codeception/base/src/Codeception/Lib/InnerBrowser.php",
    "group": "_var_www_testtaxi_vendor_codeception_base_src_Codeception_Lib_InnerBrowser_php",
    "groupTitle": "_var_www_testtaxi_vendor_codeception_base_src_Codeception_Lib_InnerBrowser_php",
    "name": "Return"
  },
  {
    "type": "",
    "url": "@var",
    "title": "\\Symfony\\Component\\BrowserKit\\Client",
    "version": "0.0.0",
    "filename": "./vendor/codeception/base/src/Codeception/Lib/InnerBrowser.php",
    "group": "_var_www_testtaxi_vendor_codeception_base_src_Codeception_Lib_InnerBrowser_php",
    "groupTitle": "_var_www_testtaxi_vendor_codeception_base_src_Codeception_Lib_InnerBrowser_php",
    "name": "Var"
  },
  {
    "type": "",
    "url": "@param",
    "title": "$locator",
    "version": "0.0.0",
    "filename": "./vendor/codeception/base/src/Codeception/Lib/Interfaces/ElementLocator.php",
    "group": "_var_www_testtaxi_vendor_codeception_base_src_Codeception_Lib_Interfaces_ElementLocator_php",
    "groupTitle": "_var_www_testtaxi_vendor_codeception_base_src_Codeception_Lib_Interfaces_ElementLocator_php",
    "name": "Param"
  },
  {
    "type": "",
    "url": "@param",
    "title": "$filename",
    "version": "0.0.0",
    "filename": "./vendor/codeception/base/src/Codeception/Lib/Interfaces/PageSourceSaver.php",
    "group": "_var_www_testtaxi_vendor_codeception_base_src_Codeception_Lib_Interfaces_PageSourceSaver_php",
    "groupTitle": "_var_www_testtaxi_vendor_codeception_base_src_Codeception_Lib_Interfaces_PageSourceSaver_php",
    "name": "Param"
  },
  {
    "type": "",
    "url": "@param",
    "title": "$filename",
    "version": "0.0.0",
    "filename": "./vendor/codeception/base/src/Codeception/Lib/Interfaces/ScreenshotSaver.php",
    "group": "_var_www_testtaxi_vendor_codeception_base_src_Codeception_Lib_Interfaces_ScreenshotSaver_php",
    "groupTitle": "_var_www_testtaxi_vendor_codeception_base_src_Codeception_Lib_Interfaces_ScreenshotSaver_php",
    "name": "Param"
  },
  {
    "type": "",
    "url": "@var",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/codeception/base/src/Codeception/Module/Db.php",
    "group": "_var_www_testtaxi_vendor_codeception_base_src_Codeception_Module_Db_php",
    "groupTitle": "_var_www_testtaxi_vendor_codeception_base_src_Codeception_Module_Db_php",
    "name": "Var"
  },
  {
    "type": "",
    "url": "@var",
    "title": "",
    "version": "0.0.0",
    "filename": "./vendor/codeception/base/src/Codeception/Module/MongoDb.php",
    "group": "_var_www_testtaxi_vendor_codeception_base_src_Codeception_Module_MongoDb_php",
    "groupTitle": "_var_www_testtaxi_vendor_codeception_base_src_Codeception_Module_MongoDb_php",
    "name": "Var"
  },
  {
    "type": "",
    "url": "@param",
    "title": "\\Closure $capabilityFunction",
    "version": "0.0.0",
    "filename": "./vendor/codeception/base/src/Codeception/Module/WebDriver.php",
    "group": "_var_www_testtaxi_vendor_codeception_base_src_Codeception_Module_WebDriver_php",
    "groupTitle": "_var_www_testtaxi_vendor_codeception_base_src_Codeception_Module_WebDriver_php",
    "name": "Param"
  },
  {
    "type": "",
    "url": "@param",
    "title": "$page WebDriver instance or an element to search within",
    "version": "0.0.0",
    "filename": "./vendor/codeception/base/src/Codeception/Module/WebDriver.php",
    "group": "_var_www_testtaxi_vendor_codeception_base_src_Codeception_Module_WebDriver_php",
    "groupTitle": "_var_www_testtaxi_vendor_codeception_base_src_Codeception_Module_WebDriver_php",
    "name": "Param"
  },
  {
    "type": "",
    "url": "@param",
    "title": "RemoteWebDriver $session",
    "version": "0.0.0",
    "filename": "./vendor/codeception/base/src/Codeception/Module/WebDriver.php",
    "group": "_var_www_testtaxi_vendor_codeception_base_src_Codeception_Module_WebDriver_php",
    "groupTitle": "_var_www_testtaxi_vendor_codeception_base_src_Codeception_Module_WebDriver_php",
    "name": "Param"
  },
  {
    "type": "",
    "url": "@param",
    "title": "$webDriver (optional) a specific webdriver session instance",
    "version": "0.0.0",
    "filename": "./vendor/codeception/base/src/Codeception/Module/WebDriver.php",
    "group": "_var_www_testtaxi_vendor_codeception_base_src_Codeception_Module_WebDriver_php",
    "groupTitle": "_var_www_testtaxi_vendor_codeception_base_src_Codeception_Module_WebDriver_php",
    "name": "Param"
  },
  {
    "type": "",
    "url": "@return",
    "title": "mixed",
    "version": "0.0.0",
    "filename": "./vendor/codeception/base/src/Codeception/Module/WebDriver.php",
    "group": "_var_www_testtaxi_vendor_codeception_base_src_Codeception_Module_WebDriver_php",
    "groupTitle": "_var_www_testtaxi_vendor_codeception_base_src_Codeception_Module_WebDriver_php",
    "name": "Return"
  },
  {
    "type": "",
    "url": "@return",
    "title": "RemoteWebDriver",
    "version": "0.0.0",
    "filename": "./vendor/codeception/base/src/Codeception/Module/WebDriver.php",
    "group": "_var_www_testtaxi_vendor_codeception_base_src_Codeception_Module_WebDriver_php",
    "groupTitle": "_var_www_testtaxi_vendor_codeception_base_src_Codeception_Module_WebDriver_php",
    "name": "Return"
  },
  {
    "type": "",
    "url": "@throws",
    "title": "ModuleException",
    "version": "0.0.0",
    "filename": "./vendor/codeception/base/src/Codeception/Module/WebDriver.php",
    "group": "_var_www_testtaxi_vendor_codeception_base_src_Codeception_Module_WebDriver_php",
    "groupTitle": "_var_www_testtaxi_vendor_codeception_base_src_Codeception_Module_WebDriver_php",
    "name": "Throws"
  }
] });

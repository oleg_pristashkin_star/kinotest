<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m190811_060338_users_table
 */
class m190811_060338_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('admin_user', [
            'id' => Schema::TYPE_PK,
            'firstName' => Schema::TYPE_STRING,
            'lastName' => Schema::TYPE_STRING,
            'username' => Schema::TYPE_STRING . ' NOT NULL',
            'password' => Schema::TYPE_STRING . ' NOT NULL',
            'authKey' => Schema::TYPE_STRING,
        ]);

        $this->insert('admin_user',[
            'firstName' => 'Ivan',
            'lastName' => 'Test',
            'username' => 'admin',
            'password' => Yii::$app->security->generatePasswordHash('admin'),
            'authKey'  => Yii::$app->security->generateRandomString()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('admin_user');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190811_060338_users_table cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%movie}}`.
 */
class m190811_093014_create_movie_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%movie}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'img' => $this->string(255),
            'price' => $this->float(),
            'date' => $this->date(),
            'info' => $this->string(512)
        ]);

        $this->insert('{{%movie}}',[
            'name' => 'Startrack',
            'img' => '/startrack23',
            'price' => '50',
            'date' => '2019-09-21',
            'info' => 'Holl #7'
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%movie}}');
    }
}

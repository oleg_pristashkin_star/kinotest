<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%ticket}}`.
 */
class m190811_092939_create_ticket_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%ticket}}', [
            'id' => $this->primaryKey(),
            'row' => $this->integer(),
            'place' => $this->integer(),
            'is_blocked' => $this->boolean(),
            'is_bought' => $this->boolean()
        ]);

        $this->createIndex('row_place', '{{%ticket}}', ['row', 'place'], true);

        $this->insert('{{%ticket}}',[
            'row' => '1',
            'place' => '1',
            'is_blocked' => '0',
            'is_bought' => '0'
        ]);

        $this->insert('{{%ticket}}',[
            'row' => '1',
            'place' => '2',
            'is_blocked' => '0',
            'is_bought' => '0'
        ]);

        $this->insert('{{%ticket}}',[
            'row' => '2',
            'place' => '1',
            'is_blocked' => '0',
            'is_bought' => '0'
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%ticket}}');
        $this->dropIndex('row_place', '{{%ticket}}');
    }
}
